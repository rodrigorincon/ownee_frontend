import axios from 'axios'
import AuthService from './auth-service.js'

const instance = axios.create({
  baseURL: 'http://localhost:3000',
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  }
})

instance.interceptors.request.use(config => {
  const token = AuthService.getToken()
  if (token) {
    config.headers.Authorization = ""+token
  }
  return config
}, error => {
  console.log("Erro no request: ", error)
  return Promise.reject(error.response)
})

instance.interceptors.response.use(response => {
  var url_parts = response.config.url.split("/")
  if(url_parts[url_parts.length - 1] === "login" ){
    AuthService.login(response.data.token, response.data.name, response.data.id)
  }
  return response
}, error => {
  console.log("Erro no response: ", error)
  return Promise.reject(error.response)
})

export default instance
