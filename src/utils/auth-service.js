const TOKEN_KEY = 'ownee_authToken'
const USER_KEY = 'ownee_me'
const USER_ID = 'ownee_id'

class AuthService {
  constrainLogin(router, redirect) {
    router.push({
      path: '/login',
      query: { redirect: JSON.stringify(redirect) }
    })
  }

  getMe() {
    return JSON.parse(localStorage.getItem(USER_KEY) )
  }

  getId() {
    return JSON.parse(localStorage.getItem(USER_ID) )
  }

  getToken() {
    return localStorage.getItem(TOKEN_KEY)
  }

  isAuthenticated() {
    return !!localStorage.getItem(TOKEN_KEY)
  }

  login(token, user, id, event = null) {
    if(token) {
      this.setToken(token)
    }
    this.setMe(user)
    this.setId(id)
  }

  logout(event = null) {
    localStorage.removeItem(TOKEN_KEY)
    localStorage.removeItem(USER_KEY)
    localStorage.removeItem(USER_ID)
  }

  setMe(user) {
    localStorage.setItem(USER_KEY, JSON.stringify(user) )
  }

  setId(id) {
    localStorage.setItem(USER_ID, JSON.stringify(id) )
  }

  setToken(token) {
    localStorage.setItem(TOKEN_KEY, token)
  }
}

export default new AuthService()
