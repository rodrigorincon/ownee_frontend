import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username: null,
    userId: null
  },
  mutations: {
    changeUsername(state, newValue){
      state.username = newValue
    },
    changeUserId(state, newValue){
      state.userId = newValue
    }
  }
})
