import Vue from 'vue'
import router from './routes.js'
import http from './utils/http.js'
import stateControl from './utils/state-control.js'
import moment from 'moment-timezone'
moment.tz.setDefault('UTC-3')

Object.defineProperty(Vue.prototype, "$moment", { get(){return moment} })
Object.defineProperty(Vue.prototype, '$http', { value: http })

new Vue({
  el: '#app',
  router,
  store: stateControl
})
