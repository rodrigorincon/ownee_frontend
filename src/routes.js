import Index from './components/Index.vue'
import Login from './components/Login.vue'
import Post from './components/Post.vue'
import Users from './components/Users.vue'

import AuthService from './utils/auth-service.js'
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
  	routes:[
		{path: '/', name: 'home', component: Index, meta: { requiresAuth: true} },
		{path: '/login', name: 'login', component: Login, meta: { requiresAuth: false} },
		{path: '/post/:id', name: 'post', component: Post, meta: { requiresAuth: true}, props: true },
		{path: '/users', name: 'users', component: Users, meta: { requiresAuth: true} },
		
		// default route, when none of the above matches (404 page)
		{ path: "*", redirect: {name: 'home'}, meta: { requiresAuth: false} }
	]
})

router.beforeEach( (to, from, next) => {
	if (to.meta.requiresAuth && !AuthService.isAuthenticated() ) {
		next({
			path: '/login'
		})
		return
	}
	next()
})

export default router